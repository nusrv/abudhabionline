<?php

class m160817_170838_catsource extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `category` ADD `lang` ENUM(\'ar\',\'en\') NULL AFTER `title`;
');
	}

	public function down()
	{
		echo "m160817_170838_catsource does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}