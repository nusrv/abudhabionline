<?php

class OldGeneratorsCommand extends BaseCommand
{
    // Please to use this command , run every 15 mint

    private $start_time;

    private $end_time;

    private $per_post_day;

    private $scheduled_counter = 0;

    private $gap_time = 0;

    private $PlatFrom ;

    private $rule;

    private $template;

    private $main_hash_tag;

    private $hash_tag;

    private $trim_hash;

    public function run()
    {
        $this->TimeZone();
        $this->template = PostTemplate::model()->findAll('deleted = 0 ORDER BY RAND()');

        if(empty($this->template))
        {
            echo 'Please insert template '.PHP_EOL;
            exit();
        }

        system("clear");

        $this->rule = Yii::app()->params['rules'];

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index=>$item) {
            $this->trim_hash[$index]=" #".str_replace(" ", "_",trim($item->title)).' ';
            //$this->hash_tag[$index]=str_replace(" ", "_",trim($item->title));
            $this->hash_tag[$index]=trim($item->title);
        }
        $this->main_hash_tag = Yii::app()->params['HashTagRoot'];

        $category = Category::model()->findAll('deleted = 0 and active =1');

        $Settings = Settings::model()->findByPk(1);

        $this->PlatFrom = Platform::model()->findAll('deleted = 0');

        $this->per_post_day = $Settings->how_many_posts;

        $this->start_time = $Settings->start_date_time;

        $this->end_time = $Settings->end_date_time;

        $this->gap_time = $Settings->gap_time;

        $criteria=New CDbCriteria();

        $criteria->compare('is_scheduled',1,true);

        $criteria->addCondition('parent_id IS NULL');
        $day   =     0;

        $today =     date('Y-m-d',strtotime(date('Y-m-d').$day.' day'));

        $start =     date("Y-m-d H:i:s",strtotime($today . ' ' . $this->start_time));

        $end   =     date("Y-m-d H:i:s",strtotime($today . ' ' . $this->end_time));

        if($end < $start)
            $criteria->addBetweenCondition('schedule_date', date('Y-m-d'), date('Y-m-d',strtotime(date('Y-m-d').' 1 day')), 'AND');
        else
            $criteria->compare('schedule_date',date('Y-m-d'),true);

        $this->scheduled_counter = PostQueue::model()->count($criteria);

        $title = 'الصفحة الرئيسة';
        $title2 = 'عرض و طلب';
        foreach ($category as $item) {
            $settings_category = Settings::model()->findByAttributes(array('category_id'=>$item->id));

            if(!empty($settings_category)){
                $this->start_time = $settings_category->start_date_time;
                $this->end_time = $settings_category->end_date_time;
                $this->gap_time = $settings_category->gap_time;
            }else{
                $this->start_time = $Settings->start_date_time;
                $this->end_time = $Settings->end_date_time;
                $this->gap_time = $Settings->gap_time;
            }

            echo  $this->start_time .' <- start_time'.PHP_EOL;
            echo  $this->end_time .'   <- end_time'.PHP_EOL;
            echo  $this->gap_time .'   <- gap_time'.PHP_EOL;
            echo  $this->per_post_day .'   <- per_post_day'.PHP_EOL;
            echo  $this->scheduled_counter .'   <- scheduled_counter'.PHP_EOL;



            $now  = strtotime(date('Y-m-d H:i:s'). ' + '.$this->gap_time.' minutes');
            $start = strtotime(date('Y-m-d').' '.$this->start_time);
            $end = strtotime(date('Y-m-d').' '.$this->end_time);

            if($end < $start){
                $extra_day = 1;
                $end = strtotime(date('Y-m-d').' '.$this->end_time.' + '.$extra_day.' day');
                echo 'END Date '.date('Y-m-d H:i:s',$end).PHP_EOL;
            }

                if(($now < $start) or ($now > $end)){
                    continue;
                }

            if($item->title != $title and $item->title != $title2){
                if(is_array($item->Subcategory) && !empty($item->Subcategory)){
                    foreach ($item->Subcategory as $sub) {
                        if($sub->active ==1 && $sub->deleted ==0)
                        $this->NewsHtmlDom($sub,1);
                    }
                }else{
                    $this->NewsHtmlDom($item,0);
                }
            }
        }
    }


    private function clear_tags($string){

        $string = strip_tags($string);
        $string = str_replace('&nbsp;','',$string);
        $string = str_replace('&raquo;','',$string);
        $string = str_replace('&laquo;','',$string);
        $string = str_replace('&quot;','',$string);
        $string = str_replace('nbsp;','',$string);
        return $string;
    }

    private function get_date($date){

        return date('Y-m-d H:i:s',strtotime($date));

    }

    private function clear_author($string){

        $string = strip_tags($string);

        return $string;
    }


    private function get_date_from_url($url)
    {
        $url = explode('/', $url);
        if (is_array($url)) {
            $url = explode('-', end($url));
            if (is_array($url) && (isset($url[0]) && isset($url[1]) && isset($url[2])) && (!empty($url[0]) && !empty($url[1]) && !empty($url[2]))) {
                return date('Y-m-d', strtotime($url[0] . '-' . $url[1] . '-' . $url[2]));
            }
        }
        return false;
    }

    private function check_news($md5_url){
        $model = News::model()->find('link_md5 = "' . $md5_url . '"');
        return array(empty($model) ? new News():News::model()->findByPk($model->id),empty($model) ? true : false);
    }

    private function get_details($url ,$category , $sub_category)
    {
        $html = new SimpleHTMLDOM();

        $html_data = $html->file_get_html($url);

        $data['url']= $url;

        $data['column']= null;

        $data['link_md5'] = md5($url);

        $data['category_id'] = $category;

        $data['sub_category_id'] = $sub_category;

        if(isset($html_data->find('h1[class=articletitle]', 0)->innertext))
            $data['title']= $this->clear_tags($html_data->find('h1[class=articletitle]', 0)->innertext);
        else {
            if(isset($html_data->find('meta[property=og:title]',0)->content))
                $data['title'] =  $this->clear_tags($html_data->find('meta[property=og:title]',0)->content);
            else
                $data['title'] = 'blank title';
        }

        $data['date'] = $this->get_date($this->clear_tags($html_data->find('time[class=date]', 0)->datetime));
        if(isset($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext))
            $data['author'] = $this->clear_author($html_data->find('div[class=pull_right] ul li[itemprop=author]', 0)->innertext);
        else{
            if(isset($html_data->find('meta[name=author]',0)->content))
                $data['author'] = $this->clear_author($html_data->find('meta[name=author]',0)->content);
            elseif(isset($html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content))
              $data['author'] = $html_data->find('section[itemprop=author] meta[itemprop=name]',0)->content;
            else
              $data['author'] = 'blank author';
        }

        $data['number_star']='';
        if(isset($html_data->find('span[id=star-raters-number]', 0)->innertext))
            $data['number_star'] = $this->clear_tags($html_data->find('span[id=star-raters-number]', 0)->innertext);

        $data['description']='';
        if(isset($html_data->find('div[id=article-body] p', 0)->innertext))
            $data['description']= $this->clear_tags($html_data->find('div[id=article-body] p', 0)->innertext);

        foreach ($html_data->find('meta') as $item) {
            if (strpos($item->getAttribute('name'), 'twitter:image:src') !== false) {
                $data['image']['src'] = $item->getAttribute('content');
                $data['image']['type']='image';
            }
        }

        if(isset($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext))
            $data['column']= $this->clear_tags($html_data->find('article h3[itemprop=alternativeHeadline]',0)->innertext);

        return $data;
    }

    private function get_details_gallery($url,$category,$sub_category)
    {
        $data = false;
        $html = new SimpleHTMLDOM();
        $html_data = $html->file_get_html($url);
        $data['column']= null;

        foreach ($html_data->find('meta') as $item) {
            if (strpos($item->getAttribute('name'), 'last-modified') !== false)
                $data['date'] =  date('Y-m-d H:i:s',strtotime($item->getAttribute('content')));
        }

        if(date('Y-m-d',strtotime($data['date'])) == date('Y-m-d')){

            $data['url'] = $url;
            $data['category_id'] = $category;
            $data['sub_category_id'] = $sub_category;
            $data['link_md5'] = md5($url);

            if(isset($html_data->find('h2[class=contenttitle]',0)->plaintext))
                $data['title'] = $html_data->find('h2[class=contenttitle]',0)->plaintext;
            else {
            if(isset($html_data->find('meta[property=og:title]',0)->content))
                $data['title'] = $html_data->find('meta[property=og:title]',0)->content;
            else
                $data['title'] = 'blank title';
            }
            $gallary = $html_data->find('div[class=imagegalleryslider] ul[class=slides] li img');
            $data['image']['type']='gallery';
            if(is_array($gallary)){
                $counter=0;
                foreach ($gallary as $item) {
                    $data['image']['src'][$counter] =  $item->src;
                    $counter++;
                }
            }
            return $data;
        }

        return null;

    }

    private function NewsHtmlDom($sub,$type)
    {

        $html = new SimpleHTMLDOM();
        $html_data = $html->file_get_html($sub->url);
        $programs = $html_data->find('div[class=m_overlay] p[class=heading overleytitle] a');

        $sub_category = null;
        $category = null;
        if($type){
            $sub_category = $sub->id;
            $category = $sub->category_id;
        }else
            $category = $sub->id;

        foreach ($programs as $program) {
            $data = null;
            $url = Yii::app()->params['feedUrl'] . $program->href;
            $md5_url = md5($url);
            $date = $this->get_date_from_url($program->href);

            if ($date == date('Y-m-d')) {
            /*if (true) {*/
                list($model, $valid) = $this->check_news($md5_url);
                if ($valid)
                    $data = $this->get_details($url , $category , $sub_category);
            }else{
                if($date == false){
                    if (!preg_match("~^(?:f|ht)tps?://~i", $program->href))
                        $data = $this->get_details_gallery(Yii::app()->params['feedUrl'].$program->href,$category , $sub_category);
                    else
                        $data =$this->get_details_gallery($program->href,$category , $sub_category);
                }
            }
            if(!empty($data)){
                $this->AddNews($data);
            }
        }

    }

    public function date(){
        $final_date = null;
        $condition = new CDbCriteria();
        $condition->order = 'id Desc';
        $condition->limit = 1;
        $condition->condition = 'is_scheduled = 1';
        $check = PostQueue::model()->find($condition);
        $is_scheduled=1;
        $now  = strtotime(date('Y-m-d H:i:s'). ' + '.$this->gap_time.' minutes');
        $start = strtotime(date('Y-m-d').' '.$this->start_time);
        $end = strtotime(date('Y-m-d').' '.$this->end_time);

        if($end < $start){
            $extra_day = 1;
            $end = strtotime(date('Y-m-d').' '.$this->end_time.' + '.$extra_day.' day');
            echo 'END Date '.date('Y-m-d H:i:s',$end).PHP_EOL;
        }

        if(empty($check)){
            if(($now >= $start) && ($now <= $end)){
                $final_date= $now;
                echo ' 1- if(($now >= $start) && ($now <= $end)){'.date('Y-m-d H:i:s',$final_date).PHP_EOL;
            }else{
                $final_date= $start;
                echo ' 1- else -> if(($now >= $start) && ($now <= $end)){'.date('Y-m-d H:i:s',$final_date).PHP_EOL;
                $is_scheduled=0;
            }
        }else{
            $date = strtotime($check->schedule_date . ' + '.$this->gap_time.' minutes');
            if($date<$now){
                $date=$now;
                echo ' 2- else -> $date<$now'.date('Y-m-d H:i:s',$date).PHP_EOL;
            }

            if(($date >= $start) && ($date <= $end)){
                $final_date= $date;
                echo ' 2- else -> if(($date >= $start) && ($date <= $end)){'.date('Y-m-d H:i:s',$final_date).PHP_EOL;
            }else{
                $is_scheduled=0;
                if(($now >= $start) && ($now <= $end)){
                    $final_date= $now;
                    echo ' 2- else -> else -> ( -> if(($date >= $start) && ($date <= $end)){'.date('Y-m-d H:i:s',$final_date).PHP_EOL;
                }else{
                    $final_date= $start;
                    echo ' 2- else -> else -> ( else  {'.date('Y-m-d H:i:s',$final_date).PHP_EOL;
                }
            }
        }

        $post_date=date('Y-m-d H:i:s',$final_date);

        return array($post_date,$is_scheduled);



    }

    public function AddNews($data){

        $shortUrl = new ShortUrl();
        $news = News::model()->findByAttributes(array('link_md5' => $data['link_md5'], 'category_id' => $data['category_id']));
        if(empty($news)){
            $news = new News();
            $news->command = false;
            $news->id = null;
            $news->link = $data['url'];
            $news->link_md5 = $data['link_md5'];
            $news->category_id = $data['category_id'];
            $news->sub_category_id = $data['sub_category_id'];
            $news->title = $data['title'];
            $news->column = $data['column'];
            $news->description = isset($data['description'])?$data['description']:null;
            $news->publishing_date = $data['date'];
            $news->schedule_date = $data['date'];
            $news->created_at = date('Y-m-d H:i:s');
            $news->creator = isset($data['author'])?$data['author']:null;
            $news->shorten_url = $shortUrl->short(urldecode($news->link));
            list($post_date,$is_scheduled)=$this->date();
            $news->schedule_date = $post_date;
            $news->setIsNewRecord(true);
            if($news->save(true))
            {
                if($this->validateDate($news->schedule_date)){
                    $this->get_mediaNews($news->id,$data['image']);
                    $post = $this->generator($news , $data['image']['type'],$is_scheduled);
                }
                return true;
            }else{
                $this->send_email($news,'error on News');

                return false;
            }
        }

        return false;
    }

    public function get_mediaNews($id, $data)
    {

        $media = new MediaNews();
        if($data['type'] == 'image'){

                $media->command=false;
                $media->id=null;
                $media->news_id = $id;
                $media->media = $data['src'];
                $media->type=$data['type'];
                $media->setIsNewRecord(true);
                if(!$media->save())
                    $this->send_email($media,'error on Media news image');


        }elseif($data['type'] == 'gallery'){
            foreach ($data['src'] as $item) {
                $media->id=null;
                $media->news_id = $id;
                $media->media = $item;
                $media->type=$data['type'];
                $media->setIsNewRecord(true);
                if(!$media->save())
                    $this->send_email($media,'error on Media news gallery');

            }
        }

    }

    public function strip_string($string)
    {
        $pattern = '/&([#0-9A-Za-z]+);/';
        $data =  preg_replace($pattern, '', $string);
        return str_replace("[]","",$data);
    }

    public function download_video($download_video_urls)
    {

        $download_video_url = "'$download_video_urls'";
        exec("youtube-dl -F $download_video_url", $out);
        if (!empty($out)) {
            if (isset(explode(' ', $out[count($out) - 1])[0])) {
                $id = explode(' ', $out[count($out) - 1])[0];
                $path = Yii::app()->params['webroot'] . '/video/' . time() . '.mp4';
                exec("youtube-dl -o '$path' $download_video_url -f '$id'", $output);
                if (strpos(end($output), '100%') !== false) {
                    list($valid, $url) = $this->upload_file($path);
                    if ($valid) {
                        return $url;
                    } else {
                        print_r($output);
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                print_r($out);
                return false;
            }
        }
        print_r($out);
        return false;
    }

    private function generator($news ,$type,$is_scheduled){



        if($is_scheduled)
        $this->scheduled_counter++;
        $twitter_is_scheduled =false;

        $parent = null;

        $post = array();

        if($this->scheduled_counter > $this->per_post_day)
            $is_scheduled = 0;



        foreach ($this->PlatFrom  as $item) {

            $pt=strtolower($item->title);
            $rule_post = ucfirst($this->rule[$pt][rand(0,count($this->rule[$pt])-1)]);
            if(!empty($rule_post)){
                $post[$item->title]=array(
                    'index'=> $item->id,
                    'type'=>$rule_post,
                );
            }
        }

        foreach ($post as $index=>$item) {
            echo $index.' - '.$is_scheduled.PHP_EOL;
            $twitter_is_scheduled =false;
            $temp = $this->get_template($item['index'],$news->category_id,$item['type']);

            $title = $news->title;

            $description = $news->description;

            $text = str_replace(
                array(
                    '[title]',
                    '[description]',
                    '[short_link]'
                ),
                array(
                    $title,
                    $description,
                    $news->shorten_url
                ),
                $temp['text']);

            if($index == 'Instagram')
            $text = str_replace(
                array(
                    '[title]',
                    '[description]',
                    '[short_link]',
                ),
                array(
                    $title,
                    $description,
                    '',
                ),
                $temp['text']);

            $media= null;
            if($type == 'image'){
                if(isset($news->mediaNews)){
                    $index_media= rand(0,count($news->mediaNews)-1);
                    $media = $news->mediaNews[$index_media]->media;
                }
            }
            $category = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_', $news->category->title));
            $column = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_', $news->column));
            $subCategory = '#'.trim(str_replace(array('!','"',':','.','..','...',' '), '_', isset($news->subCategory->title)?$news->subCategory->title:null));
            $creator = trim(str_replace(array('!','"',':','.','..','...'), '', $news->creator));


            if($index == 'Twitter'){
               // $text = $this->shorten($text,'60',false);
                $text_twitter = $text;
                if($news->category->title == 'أعمدة')
                    $text =  $text.PHP_EOL.$this->main_hash_tag['ar'].' | '.Yii::app()->params['line'].':'.$creator;
                else
                    $text =  $text.PHP_EOL.$this->main_hash_tag['ar'].' | '.Yii::app()->params['source'].':'.$creator;

                if($post[$index]['type'] == 'Preview'){
                    if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                    {
                        $text .=PHP_EOL.$news->shorten_url;
                    }
                }

                if ($this->getTweetLength($text,$post[$index]['type'] == 'Image'?true:false,$post[$index]['type'] == 'Video'?true:false) > 141) {
                    if ($this->getTweetLength($text_twitter,$post[$index]['type'] == 'Image'?true:false,$post[$index]['type'] == 'Video'?true:false) > 141) {
                        $is_scheduled = 0;
                        $twitter_is_scheduled = true;
                    }else{
                        $twitter_is_scheduled =false;
                        $text = $text_twitter.PHP_EOL;

                        $len = $this->getTweetLength($text.$this->main_hash_tag['ar'],$post[$index]['type'] == 'Image'?true:false,$post[$index]['type'] == 'Video'?true:false);
                        if ((141 > $len)) {
                            $text = $text.Yii::app()->params['HashTagRoot']['ar'];
                        }

                        if($post[$index]['type'] == 'Preview'){
                            if(!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!',$text,$matches))
                            {
                                $text .=PHP_EOL.$news->shorten_url;
                            }
                        }
                    }
                }


            }else{
                if($twitter_is_scheduled){
                    if(!$is_scheduled){
                        $is_scheduled = 1;
                    }
                }
                if($news->category->title == 'أعمدة')
                    $text =  $text.PHP_EOL.$this->main_hash_tag['ar'].' | '.$category.' | '.$column.' | '.Yii::app()->params['line'].':'.$creator;
                else
                    $text =  $text.PHP_EOL.$this->main_hash_tag['ar'].' | '.$category.' | '.$subCategory.' | '.Yii::app()->params['source'].':'.$creator;
            }

            $text = str_replace($this->hash_tag, $this->trim_hash, $text);
            $text = str_replace('# #', '#', $text);

            $PostQueue = new PostQueue();
            $PostQueue->setIsNewRecord(true);
            $PostQueue->id= null;
            $PostQueue->command= false;
            $PostQueue->type = $post[$index]['type'];
            $PostQueue->post = $text;
            $PostQueue->schedule_date = $news->schedule_date;
            $PostQueue->catgory_id = $news->category_id;
            $PostQueue->link = $news->shorten_url;
            $PostQueue->is_posted = 0;
            $PostQueue->news_id =$news->id;
            $PostQueue->post_id =null;
            $PostQueue->media_url =$media;
            $PostQueue->is_scheduled =$is_scheduled;
            $PostQueue->platform_id =$post[$index]['index'];
            $PostQueue->generated ='auto';
            $PostQueue->created_at =date('Y-m-d H:i:s');
            if($parent == null){
                $PostQueue->parent_id =null;
                if($PostQueue->save())
                $parent = $PostQueue->id;
                else
                    $this->send_email($parent,'error on post queue parent');

            }else{
                $PostQueue->parent_id =$parent;
                if(!$PostQueue->save())
                    $this->send_email($PostQueue,'error on post queue parents not null');

            }
        }

        $news->generated = 1;
        if(!$news->save())
            $this->send_email($news,'error on news');


    }

    private function get_template($platform,$category,$type){

        $cond = true;

        $temp = null;

        $counter_temp = count($this->template)-1;

        $counter=0;

        while($cond){

            $rand = rand(0,$counter_temp);

            $temp = $this->template[$rand];

            if($temp->submitToAll == 1 && $temp->type == $type){
                $cond = false;
            }

            if($temp->submitToAll == 0 && $temp->platform_id == $platform  && $temp->catgory_id == $category && $temp->type == $type ){
                $cond = false;
            }

            if($counter == $counter_temp){
                $cond = false;
                $temp = Yii::app()->params['templates'];
            }

        }

        return $temp;
    }




}