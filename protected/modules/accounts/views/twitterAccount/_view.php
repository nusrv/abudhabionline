<?php
/* @var $this TwitterAccountController */
/* @var $data TwitterAccount */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link_page')); ?>:</b>
	<?php echo CHtml::encode($data->link_page); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('twitter_key')); ?>:</b>
	<?php echo CHtml::encode($data->twitter_key); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secret')); ?>:</b>
	<?php echo CHtml::encode($data->secret); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('token')); ?>:</b>
	<?php echo CHtml::encode($data->token); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_general')); ?>:</b>
	<?php echo CHtml::encode($data->is_general); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('token_secret')); ?>:</b>
	<?php echo CHtml::encode($data->token_secret); ?>
	<br />


</div>