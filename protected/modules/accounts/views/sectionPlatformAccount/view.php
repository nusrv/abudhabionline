<?php
/* @var $this SectionPlatformAccountController */
/* @var $model SectionPlatformAccount */

$this->breadcrumbs=array(
	'Section Platform Accounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SectionPlatformAccount', 'url'=>array('index')),
	array('label'=>'Create SectionPlatformAccount', 'url'=>array('create')),
	array('label'=>'Update SectionPlatformAccount', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SectionPlatformAccount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SectionPlatformAccount', 'url'=>array('admin')),
);
?>

<h1>View SectionPlatformAccount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'section_id',
		'platform_id',
		'account_id',
	),
)); ?>
