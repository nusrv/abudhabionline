<?php
/* @var $this LogsController */
/* @var $model Logs */
/* @var $form TbActiveForm */

$this->pageTitle = "Logs | Admin";

$this->breadcrumbs=array(
	'sections'=>array('admin'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#logs-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
/*$(\"input[name='Logs[title]']\").attr('class','form-control');

$(\"select[name='Logs[active]']\").attr('class','form-control');*/
");
?>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-12 pull-right">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>
					</div>
				</div>
				<div class="box-body">
					<?php
					$this->widget('booster.widgets.TbGridView', array(
							'id'=>'logs-grid',
							'type' => 'striped bordered condensed',
							'template' => '{items}{pager}{summary}',
							'enablePagination' => true,
							'pager' => array(
								'class' => 'booster.widgets.TbPager',
								'nextPageLabel' => 'Next',
								'prevPageLabel' => 'Previous',
								'htmlOptions' => array(
									'class' => 'pull-right',
									'filterClass'=>'asdasdasd',
								),

							),
							'htmlOptions' => array(
								'class' => 'table-responsive'
							),
							'filter' => $model,
							'dataProvider' => $model->search(),
							'columns' => array(
								array(
                                    'name'=>'id',
                                ),
								array(
									'name'=>'controller',
									'type'=>'raw',

								),	array(
									'name'=>'action',
									'type'=>'raw',

								),
							array(
									'name'=>'row_id',
									'type'=>'raw',

								),	array(
									'name'=>'user_id',
									'value'=>'$data->user->username',
									'type'=>'raw',

								),	array(
									'name'=>'created_at',
									'type'=>'raw',

								),




								array(
									'class' => 'booster.widgets.TbButtonColumn',
									'header' => 'Options',
									//'template' => '{view}{update}{delete}{activate}{deactivate}',
									//'template' => '{view}{delete}',
									'template' => '{view}',
									'buttons' => array(
										'view' => array(
											'label' => 'View',
											'icon' => 'fa fa-eye',
											'buttonType'=>'url',
											'url'=>'Yii::app()->baseUrl."/".$data->controller."/".$data->action."/".$data->row_id',
											'options' => array('target' => '_new'),
										),

										'delete' => array(
											'label' => 'Delete',
											'icon' => 'fa fa-times',
										),
									)
								),
							))

					);?>
					<style>
						.checkbox {
							display: block;
							min-height: 0px;
							margin-top: 4px;
							margin-bottom: 0px;
							padding-left: 17px;
						}
						.input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
							margin-top: 0;
						}
						.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
							float: left;
							margin-left: -16px;
						}
					</style>



				</div>

			</div>
		</div>
	</div>
</section>
