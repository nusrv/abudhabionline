<?php
/* @var $this SubCategoriesController */
/* @var $model SubCategories */

$this->pageTitle = "Sub sections | Admin";

$this->breadcrumbs = array(
    'Sub Sections' => array('admin'),
    'Manage',
);
Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#sub-categories-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
$(\"input[name='SubCategories[title]']\").attr('class','form-control');
$(\"select[name='SubCategories[active]']\").attr('class','form-control');
");
?>
<style>
    .checkbox {
        display: block;
        min-height: 0px;
        margin-top: 4px;
        margin-bottom: 0px;
        padding-left: 17px;
    }
    .input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
        margin-top: 0;
    }
    .radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
        float: left;
        margin-left: -16px;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">

                    <div class="col-sm-9 pull-right" style="padding-top: 19px; text-align:left;">
                      <?php echo Yii::app()->params['statement']['previousPage']; ?>


                    </div>
                </div>
                <div class="box-body">

                    <?PHP //echo $this->renderPartial('_search',array('model'=>$model),true) ?>

                    <?PHP
                    $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'id'=>'form-visible',
                    ));
                    ?>
                    <div class="col-sm-2 pull-left page-sizes"  >

                        <?php echo $form->dropDownListGroup(
                            $model,
                            'pagination_size',
                            array(

                                'widgetOptions'=>array(
                                    'data'=>$model->pages_size(),
                                    'htmlOptions'=>array(

                                    ),
                                ),
                                'hint'=>''
                            )
                        ); ?>
                    </div>
                    <?php
                    $this->endWidget();
                    $this->widget('booster.widgets.TbGridView', array(
                        'id' => 'category-grid',
                        'type' => 'striped bordered condensed',
                        'template' => '{items}{summary}{pager}',
                        'enablePagination' => true,
                        'pager' => array(
                            'class' => 'booster.widgets.TbPager',
                            'nextPageLabel' => 'Next',
                            'prevPageLabel' => 'Previous',
                            'htmlOptions' => array(
                                'class' => 'pull-right'
                            )
                        ),
                        'htmlOptions' => array(
                            'class' => 'table-responsive'
                        ),
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => array(
                         /*   array(
                                'name'=>'id',
                                'visible'=>$model->visible_id?true:false,
                            ),*/
                           /* array(
                                'name'=>'title',
                                //'filter'=>CHtml::listData(SubCategories::model()->findAll('deleted=0'),'title','title'),
                                'type'=>'raw',
                                'visible'=>$model->visible_title?true:false,

                            ),*/
                            array(
                                'class' => 'booster.widgets.TbEditableColumn',
                                'name' => 'title',
                                'sortable' => true,
                                'editable' => array(
                                    'url' => $this->createUrl('subCategories/edit'),
                                    'placement' => 'right',
                                    'inputclass' => 'span1'
                                ),
                                'visible'=>$model->visible_title?true:false,
                            ),
                            array(
                                'class' => 'booster.widgets.TbEditableColumn',
                                'name' => 'active',
                                'filter'=>array_merge(array(''=>'All status'),array('0'=>'Disabled','1'=>'Active')),
                                'sortable' => true,
                                'editable' => array(
                                    'type' => 'select2',
                                    'url' => $this->createUrl('subCategories/edit'),
                                    'placement' => 'right',
                                    'inputclass' => 'span3',
                                    'source' => array('0'=>'Disabled','1'=>'Active'),
                                ),
                                'type'=>'raw',
                                'value'=>'$data->active?CHtml::tag("span",array("class"=>"label label-success"),"Active",true):CHtml::tag("span",array("class"=>"label label-danger"),"Disabled",true)',
                                'visible'=>$model->visible_title?true:false,                            ),

                            array(
                                'name' => 'category_id',
                                'type' => 'raw',
                                'filter' => CHtml::listData(Category::model()->findAll('deleted=0'), 'id', 'title'),
                                'value' => '$data->category->title',
                                'visible'=>$model->visible_category_id?true:false,

                            ),



/*
                            array(
                                'name'=>'active',
                              //  'filter'=>'',
                                'type'=>'raw',
                                'value'=>'$data->active?CHtml::tag("span",array("class"=>"label label-success"),"Active",true):CHtml::tag("span",array("class"=>"label label-danger"),"Disabled",true)',
                                'visible'=>$model->visible_active?true:false,
                            ),*/

                       /*     array(
                                'name'=>'created_at',
                                'visible'=>$model->visible_created_at?true:false,
                            ),*/


                             array(
                                'class' => 'booster.widgets.TbButtonColumn',
                                'header' => 'Options',
                                //'template' => '{view}{update}{delete}{activate}{deactivate}',
                                'template' => '{delete}{activate}{deactivate}',
                                 'buttons' => array(
                                     'view' => array(
                                         'label' => 'View',
                                         'icon' => 'fa fa-eye',
                                     ),
                                     'update' => array(
                                         'label' => 'Update',
                                         'icon' => 'fa fa-pencil-square-o',
                                     ),
                                     'delete' => array(
                                         'label' => 'Delete',
                                         'icon' => 'fa fa-times',
                                     ),

                                     'activate' => array(
                                         'label' => 'Deactivate',
                                         'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/status", array("id"=>$data->id))',
                                         'icon' => 'fa fa-toggle-off',
                                         'visible' => '$data->active == 1',
                                         'click' => "function(){

				$.fn.yiiGridView.update('category-grid', {  //change my-grid to your grid's name
				type:'POST',
				url:$(this).attr('href'),
				success:function(data) {
				$.fn.yiiGridView.update('category-grid');}});return false;}",
                                     ),
                                     'deactivate' => array(
                                         'label' => 'Activate',
                                         'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/status", array("id"=>$data->id))',
                                         'icon' => 'fa fa-toggle-on',
                                         'visible' => '$data->active == 0',
                                         'click' => "function(){
				$.fn.yiiGridView.update('category-grid', {  //change my-grid to your grid's name
				type:'POST',
				url:$(this).attr('href'),
				success:function(data) {
				$.fn.yiiGridView.update('category-grid');}});return false;}"
                                     ),

                                 )
                            ),
                        ))); ?>
</div>
                </div>
            </div>
        </div>
</section>