<?php
/* @var $form TbActiveForm */
$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'form-visible',
)); ?>
    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'id',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_id',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(

                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_id?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'title',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_title',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                               // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(

                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_title?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div><div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'platform_id',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_platform_id',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                               // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(

                    'data'=>CHtml::listData(Platform::model()->findAll('title != "Instagram"'),'id','title'),
                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_platform_id?false:true,
                        'empty'=>'Choose One',
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'is_posted',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_is_posted',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(

                    'data'=>array('0'=>'Not posted','1'=>'Posted'),
                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_is_posted?false:true,
                        'empty'=>'All profile pic',
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->datePickerGroup(
            $model,
            'schedule_date',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_schedule_date',
                    array(
                        'inline'=>true,
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'options' => array(
                        'language' => 'en',
                        'format' => 'yyyy-mm-dd',
                        'viewformat' => 'yyyy-mm-dd',
                    ),

                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_schedule_date?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>


    <div class="col-sm-2">
        <?php echo $form->datePickerGroup(
            $model,
            'created_at',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_created_at',
                    array(
                        'inline'=>true,
                         'widgetOptions'=>array(
                            'htmlOptions'=>array(
                             ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'options' => array(
                        'language' => 'en',
                        'format' => 'yyyy-mm-dd',
                        'viewformat' => 'yyyy-mm-dd',
                    ),

                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_created_at?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-12">
        <?php echo  $form->checkboxGroup(
                    $model,
                    'visible_media_url',
                    array(
                        'inline'=>true,
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                            ),
                        ),

                    )
                );
        ?>

    </div>

    <div class="col-sm-2 pull-left page-sizes"  >

        <?php echo $form->dropDownListGroup(
            $model,
            'pagination_size',
            array(

                'widgetOptions'=>array(
                    'data'=>$model->pages_size(),
                    'htmlOptions'=>array(

                    ),
                ),
                'hint'=>''
            )
        ); ?>
    </div>


<?php $this->endWidget(); ?>