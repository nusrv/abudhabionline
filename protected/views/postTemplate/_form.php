<?php
/* @var $this PostTemplateController */
/* @var $model PostTemplate */
/* @var $form TbActiveForm */
$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'post-template-form',
	'enableAjaxValidation'=>true,
));
$field = 'col-sm-10';
$label = 'col-sm-2';

if($model->isNewRecord){
	$array = Yii::app()->params['rule_template']['facebook'];
}elseif($model->platform_id == 0){
	$array = Yii::app()->params['rule_template']['facebook'];
}else{
	$array = Yii::app()->params['rule_template'][strtolower($model->platform->title)];

}

?>

<?php echo $form->errorSummary($model); ?>

	<div class="col-sm-8">
		<?php echo $form->textAreaGroup($model,'text',
			array(
				'widgetOptions'=>array(
					'htmlOptions'=>array(
						'style'=>'height: 418px;',
						'id'=>'text_area'

					),
				),
				'groupOptions'=>array(
					'label'=>''

				),
			)); ?>
	</div>
	<div class="col-sm-4">
		<br>
		<br>
		<div class="row" style="text-align: center;">
			<?PHP
			echo TbHtml::button('<i class="fa fa-font" aria-hidden="true"></i>Title',array(
				'class'=>'btn btn-app',
				'onclick'=>'javascript:App.add("[title]")',
			));
			echo TbHtml::button('<i class="fa fa-align-justify"></i>Description',array(
				'class'=>'btn btn-app',
				'onclick'=>'javascript:App.add("[description]")',
			));
			echo TbHtml::button('<i class="fa fa-link"></i>short link',array(
				'class'=>'btn btn-app',
				'onclick'=>'javascript:App.add("[short_link]")',
			));
			echo TbHtml::button('<i class="fa fa-hashtag"></i>Section',array(
				'class'=>'btn btn-app',
				'onclick'=>'javascript:App.add("[section]")',
			));echo TbHtml::button('<i class="fa fa-hashtag"></i>Sub section',array(
				'class'=>'btn btn-app',
				'onclick'=>'javascript:App.add("[sub_section]")',
			));echo TbHtml::button('<i class="fa fa-hashtag"></i>Author',array(
				'class'=>'btn btn-app',
				'onclick'=>'javascript:App.add("[author]")',
			));
			echo TbHtml::button('<i class="fa fa-plus-square"></i>New line',array(
				'class'=>'btn btn-app',
				'onclick'=>'javascript:App.add("[new_line]")',
			));

			echo TbHtml::button('<i class="fa fa-behance"></i>|',array(
				'class'=>'btn btn-app',
				'onclick'=>'javascript:App.add(" | ")',
			));

			?>
		</div>

		<?PHP
		echo $form->dropDownListGroup($model,'type',array('rows'=>6, 'cols'=>50,
			'widgetOptions'=>array(
				'data'=>$array,
				'empty'=>'Choose One'
			),
			'groupOptions' => array(
				'id'=>'type_PostTemplate'
			),
			'labelOptions' => array(
				'class' => $label,
			),
			'wrapperHtmlOptions' => array(
				'class' => $field,
			),
		));

		echo $form->dropDownListGroup($model,'platform_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Platform::model()->findAll('deleted=0'),'id','title'),

				'htmlOptions'=>array(
					'onchange'=>'javascript:App.platform()',
					'empty' => 'Submit to all',

				),
			),
			'wrapperHtmlOptions' => array(
				'class' => $field,
			),
			'labelOptions' => array(
				'class' => $label,
			),
			'groupOptions'=>array(
				'class'=>'platform'
			)
		));


		echo $form->dropDownListGroup($model,'catgory_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Category::model()->findAll('deleted=0'),'id','title'),
				'htmlOptions'=>array(
					'empty' => 'Submit to all',

				),
			),

			'wrapperHtmlOptions' => array(
				'class' => $field,
			),
			'labelOptions' => array(
				'class' => $label,
			),
			'groupOptions'=>array(
				'class'=>'category'
			)
		));


		?>
	</div>
	<div class="row">
		<div class="form-actions   pull-right" style="margin-bottom: 20px;margin-right:14px;">
			<?php $this->widget(
				'booster.widgets.TbButton',
				array(
					'buttonType' => 'submit',
					'context' => 'primary',
					'label' => $model->isNewRecord ? 'Create' : 'Save'
				)
			);
			?>
		</div>
	</div>
<?php $this->endWidget(); ?>