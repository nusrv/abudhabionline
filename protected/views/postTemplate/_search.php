<?php
/* @var $form TbActiveForm */
$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'form-visible',
)); ?>
    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'id',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_id',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(

                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_id?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'type',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_type',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'data'=>array_merge(array(''=>'--choose--'),CHtml::listData(PostTemplate::model()->findAll(),'type','type')),

                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_type?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div><div class="col-sm-2">
    <?php echo $form->dropDownListGroup(
        $model,
        'platform_id',
        array(
            'prepend' => $form->checkboxGroup(
                $model,
                'visible_platform_id',
                array(
                    'inline'=>true,
                    //'labelOptions'=>array('class'=>'btn btn-primary'),
                    'widgetOptions'=>array(
                        'htmlOptions'=>array(
                            // 'style'=>'opacity: 0;'
                        ),
                    ),
                )
            ),
            'widgetOptions'=>array(

                'data'=>CHtml::listData(Platform::model()->findAll('title != "Instagram"'),'id','title'),
                'htmlOptions'=>array(
                    'disabled'=>$model->visible_platform_id?false:true,
                    'empty'=>'Choose One',
                )
            ),
            'hint'=>''
        )
    ); ?>

</div>
    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'catgory_id',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_catgory_id',
                    array(
                        'inline'=>true,
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'data'=>CHtml::listData(Category::model()->findAll('deleted=0'),'id','title'),
                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_catgory_id?false:true,
                        'empty'=>'All categories'
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>


    <div class="col-sm-2">
        <?php echo $form->datePickerGroup(
            $model,
            'created_at',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_created_at',
                    array(
                        'inline'=>true,
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'options' => array(
                        'language' => 'en',
                        'format' => 'yyyy-mm-dd',
                        'viewformat' => 'yyyy-mm-dd',
                    ),

                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_created_at?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
      <?php
      echo $form->checkboxGroup(
                    $model,
                    'visible_text',
                    array(
                        'inline'=>false,
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                            ),
                        ),
                        'div_checkbox'=>array(
                            'style'=>'margin-top:27px;'
                        )
                    )
                );
       ?>

    </div>

    <div class="col-sm-2 pull-left page-sizes"  >

        <?php echo $form->dropDownListGroup(
            $model,
            'pagination_size',
            array(

                'widgetOptions'=>array(
                    'data'=>$model->pages_size(),
                    'htmlOptions'=>array(

                    ),
                ),
                'hint'=>''
            )
        ); ?>
    </div>



<?php $this->endWidget(); ?>