<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ProfileForm extends CFormModel
{
     public $password;
    public $new_password;
    public $confirm_password;
     /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('new_password,confirm_password', 'required','on'=>'updatePassword'),
            array('confirm_password', 'compare', 'compareAttribute'=>'new_password','on'=>'updatePassword'),

        );
    }
    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'password' => 'Old password',
            'new_password' => 'New password',
            'confirm_password'   => 'Confirm password',
         );
    }


}
